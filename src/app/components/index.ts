export * from './instituciones/instituciones.component';
export * from './barrio/barrio.component';
export * from './ocupacion/ocupacion.component';
export * from './menu/menu.component';
export * from './login/login.component';
export * from './egresados/egresados.component';
