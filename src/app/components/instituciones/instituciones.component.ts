import { Component, OnInit } from '@angular/core';
import { InstitucionesService } from "../../services/instituciones.service";
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-instituciones',
  templateUrl: './instituciones.component.html',
  styleUrls: ['./instituciones.component.css']
})
export class InstitucionesComponent implements OnInit {

    instituciones : any = [];
    institucion : any = {};
    file : any;
    uploadForm: FormGroup;
    page = 1;
    pageSize = 10;

  constructor(
      private institucionesService: InstitucionesService,
      private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
      this.findAll();
      this.uploadForm = this.formBuilder.group({
          file: ['']
        });
  }

  get institucionesMap(): any {
    return this.instituciones
      .map((institucion, i) => ({id: i + 1, ...institucion}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  preInsert(){
      if(this.institucion.id == null){
          this.crearInstitucion();
      } else {
          this.updateInstitucion();
      }
  }

  crearInstitucion(){
      this.institucionesService.insertInstitucion(this.institucion).subscribe(
          response => {
              this.findAll();
          }, err =>{}
      )
      this.institucion = {};
  }

  findAll(){
      this.institucionesService.findAllInstituciones().subscribe(
          response => {
              this.instituciones = response;
          }, err =>{}
      )
  }

  updateInstitucion(){
      this.institucionesService.updateInstitucion(this.institucion).subscribe(
          response => {
              this.findAll();
          }, err =>{}
      )
      this.institucion = {};
  }

  deleteInstitucion(institucion){
      this.institucionesService.deleteInstitucion(institucion).subscribe(
          response => {
              this.findAll();
          }, err =>{}
      )
  }

  preUpdate(institucion){
      this.institucion = Object.assign({}, institucion);
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get('file').setValue(file);
    }
  }

  uploadInstituciones(){
      const formData = new FormData();
      formData.append('file', this.uploadForm.get('file').value);

      this.institucionesService.uploadInstituciones(formData).subscribe(
          response => {
              this.findAll();
          }, err => {
              console.log(err);
          }
      )
  }

}
