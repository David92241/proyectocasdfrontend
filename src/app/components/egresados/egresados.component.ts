import { Component, OnInit } from '@angular/core';
import { EgresadosService } from "../../services/egresados.service";
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'egresados',
  templateUrl: 'egresados.component.html',
})
export class EgresadosComponent implements OnInit {
    egresados : any = [];
    egresado : any = {};
    file : any;
    uploadForm: FormGroup;
    page = 1;
    pageSize = 10;
    anosEgresados: any = [2019,2018,2017,2016,2015,2016,2015,2014,2013,2012,2011,2010,2009];

  constructor(
      private egresadosService: EgresadosService,
      private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
      this.findAll();
      this.uploadForm = this.formBuilder.group({
          file: ['']
        });
  }

  get egresadossMap(): any {
    return this.egresados
      .map((egresado, i) => ({id: i + 1, ...egresado}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  preInsert(){
      if(this.egresados.id == null){
          this.crearEgresado();
      } else {
          this.updateEgresado();
      }
  }

  crearEgresado(){
      this.egresadosService.insertEgresado(this.egresado).subscribe(
          response => {
              this.findAll();
          }, err =>{}
      )
      this.egresado = {};
  }

  findAll(){
      this.egresadosService.findAllEgresados().subscribe(
          response => {
              this.egresados = response;
          }, err =>{}
      )
  }

  updateEgresado(){
      this.egresadosService.updateEgresado(this.egresado).subscribe(
          response => {
              this.findAll();
          }, err =>{}
      )
      this.egresado = {};
  }

  deleteEgresado(egresado){
      this.egresadosService.deleteEgresado(egresado).subscribe(
          response => {
              this.findAll();
          }, err =>{}
      )
  }

  preUpdate(egresado){
      this.egresado = Object.assign({}, egresado);
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get('file').setValue(file);
    }
  }

  uploadEgresados(){
      const formData = new FormData();
      formData.append('file', this.uploadForm.get('file').value);

      this.egresadosService.uploadEgresados(formData).subscribe(
          response => {
              this.findAll();
          }, err => {
              console.log(err);
          }
      )
  }
}
