import {Component, OnInit} from '@angular/core';
import {BarrioService} from "../../services/barrio.service";
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
	selector: 'app-barrio',
	templateUrl: './barrio.component.html',
	styleUrls: ['./barrio.component.css']
})
export class BarrioComponent implements OnInit {

	barrios : any = [];
	nbarrio : any = {};
	file : any;
	uploadForm: FormGroup;

	constructor( 
		private barrioService: BarrioService, 
		private formBuilder: FormBuilder
		) {
	}

	ngOnInit(){
		this.findAll();
		this.uploadForm = this.formBuilder.group({
			file: ['']
		});
	}

	preInsert(){
		if (this.nbarrio.Bar_id == null) {
			this.crearBarrio();
		}else{
			this.updateBarrio();
		}
	}

	crearBarrio(){
		this.barrioService.insertBarrio(this.nbarrio).subscribe(
			response => {
				this.findAll();
			}, err => {}
		)
		this.nbarrio = {};
	}

	findAll(){
		this.barrioService.findAllBarrios().subscribe(
			response => {
				this.barrios = response;
			}, err =>{}

		)
	}

	updateBarrio(){
		this.barrioService.updateBarrio(this.nbarrio).subscribe(
			response => {
				this.findAll();
			}, err => {}
		)
		this.nbarrio = {};
	}

	deleteBarrio(barrio){
		this.barrioService.deleteBarrio(barrio).subscribe(
			response => {
				this.findAll();
			}, err => {}
		)
	}
	preUpdate(barrio){
		this.nbarrio = Object.assign({}, barrio);
	}

	onFileSelect(event){
		if (event.target.files.length > 0) {
			const file = event.target.files[0];
			this.uploadForm.get('file').setValue(file);
		}
	}

	uploadBarrio(){
		const formData = new FormData();
		formData.append('file', this.uploadForm.get('file').value);

		this.barrioService.uploadBarrio(formData).subscribe(
			response => {
            	this.findAll();
          	}, err => {
            	console.log(err);
          		}
		)
	}
}
