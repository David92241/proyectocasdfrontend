import {Component, OnInit} from '@angular/core';
import {OcupacionService} from "../../services/ocupacion.service";
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
	selector: 'app-ocupacion',
	templateUrl: './ocupacion.component.html',
	styleUrls: ['./ocupacion.component.css']
})
export class OcupacionComponent implements OnInit {
	ocupaciones : any = [];
	nocupacion : any = {};
	file : any;
	uploadForm : FormGroup;

	constructor(
		private ocupacionService: OcupacionService,
		private formBuilder: FormBuilder
		) {
	}

	ngOnInit(){
		this.findAll();
		this.uploadForm = this.formBuilder.group({
			file: ['']
		});
	}

	preInsert(){
		if (this.nocupacion.ocupa_id == null) {
			this.crearOcupacion();
		} else {
			this.updateOcupacion();
		}
	}

	crearOcupacion(){
		this.ocupacionService.insertOcupacion(this.nocupacion).subscribe(
			response => {
				this.findAll();
			}, err => {}
		)
		this.nocupacion = {};
	}

	findAll(){
		this.ocupacionService.findAllOcupaciones().subscribe(
			response => {
				this.ocupaciones = response;
			}, err => {}
		)
	}

	updateOcupacion(){
		this.ocupacionService.updateOcupacion(this.nocupacion).subscribe(
			response => {
				this.findAll();
			}, err => {}
		)
	}

	deleteOcupacion(ocupacion){
		this.ocupacionService.deleteOcupacion(ocupacion).subscribe(
			response => {
				this.findAll();
			}, err => {}
		)
	}

	 preUpdate(ocupacion){
      this.nocupacion = Object.assign({}, ocupacion);
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get('file').setValue(file);
    }
  }

  uploadOcupacion(){
      const formData = new FormData();
      formData.append('file', this.uploadForm.get('file').value);

      this.ocupacionService.uploadOcupacion(formData).subscribe(
          response => {
              this.findAll();
          }, err => {
              console.log(err);
          }
      )
  }
}