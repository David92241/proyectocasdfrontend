import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/index';
import { Router } from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    usuario : any = {}

    constructor(
        private usuarioService: UsuarioService,
        private router: Router
    ) {
    }

    ngOnInit(){
    }

    login(){
        this.usuario.contrasena = this.usuarioService.SHA256(this.usuario.contrasena).toUpperCase();
        this.usuarioService.login(this.usuario).subscribe(
            response => {
                this.usuario = response;
                localStorage.setItem("usuario", JSON.stringify(response));
                localStorage.setItem("token", this.usuario.token);
                this.router.navigate(["home"]);
                this.usuario = {}
            }, err => {
                this.usuario = {};
                console.log('error', err);
            }
        );
    }
}
