import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {NoSecurityGuard} from './guard/noSecurity.guard';
import {SecurityGuard} from './guard/security.guard'
import {
    InstitucionesComponent,
    BarrioComponent,
    OcupacionComponent,
    MenuComponent,
    LoginComponent,
    EgresadosComponent
} from './components/index';

const routes : Routes = [
    {path: 'egresados', component: EgresadosComponent, canActivate: [SecurityGuard]},
    {path: 'instituciones', component: InstitucionesComponent, canActivate: [SecurityGuard]},
    {path: 'barrio', component: BarrioComponent, canActivate: [SecurityGuard]},
    {path: 'ocupacion', component: OcupacionComponent, canActivate: [SecurityGuard]},
    {path: 'home', component: MenuComponent, canActivate: [SecurityGuard]},
    {path: 'login', component: LoginComponent, canActivate: [NoSecurityGuard]},
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: '**', redirectTo: 'not-found', pathMatch: 'full'}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
