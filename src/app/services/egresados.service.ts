import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EgresadosService {

    private alive: boolean = true;

    constructor(private http: HttpClient) {
    }

    findAllEgresados() {
        return this.http.post(environment.apiUrl + "Egresados/findAll/", {});
    }

    insertEgresado(egresado){
        return this.http.post(environment.apiUrl + "Egresados/", egresado);
    }

    updateEgresado(egresado){
        return this.http.post(environment.apiUrl + "Egresados/updateEgresado", egresado);
    }

    deleteEgresado(egresado){
        return this.http.post(environment.apiUrl + "Egresados/deleteInstitucion", egresado);
    }

    uploadEgresados(file){
        return this.http.put(environment.apiUrl + "Egresados/uploadEgresados", file);
    }
}
