import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from '../../environments/environment';

@Injectable({
	providedIn: 'root'
})

export class OcupacionService {
	private alive: boolean = true;
	
	constructor(private http: HttpClient) {
	}

	findAllOcupaciones(){
		return this.http.post(environment.apiUrl + "Ocupacion/findAll/", {});
	}

	insertOcupacion(ocupacion){
		return this.http.post(environment.apiUrl + "Ocupacion/", ocupacion);
	}

	updateOcupacion(ocupacion){
		return this.http.post(environment.apiUrl + "Ocupacion/updateOcupacion", ocupacion);
	}

	deleteOcupacion(ocupacion){
		return this.http.post(environment.apiUrl + "Ocupacion/deleteOcupacion", ocupacion);
	}

	uploadOcupacion(file){
		return this.http.post(environment.apiUrl + "Ocupacion/uploadOcupacion", file);
	}
}