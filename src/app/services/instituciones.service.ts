import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InstitucionesService {

    private alive: boolean = true;

    constructor(private http: HttpClient) {
    }

    findAllInstituciones() {
        return this.http.post(environment.apiUrl + "Instituciones/findAll/", {});
    }

    insertInstitucion(institucion){
        return this.http.post(environment.apiUrl + "Instituciones/", institucion);
    }

    updateInstitucion(institucion){
        return this.http.post(environment.apiUrl + "Instituciones/updateInstitucion", institucion);
    }

    deleteInstitucion(institucion){
        return this.http.post(environment.apiUrl + "Instituciones/deleteInstitucion", institucion);
    }

    uploadInstituciones(file){
        return this.http.put(environment.apiUrl + "Instituciones/uploadInstituciones", file);
    }
}
