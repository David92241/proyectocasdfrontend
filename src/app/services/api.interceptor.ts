import { HttpInterceptor, HttpRequest } from '@angular/common/http';

export class ApiInterceptor implements HttpInterceptor {

  constructor() {
  }

  intercept(request: HttpRequest<any>, next) {
    const authRequest = request.clone({
      headers: request.headers.set(
        'Authorization', localStorage.getItem("token")
      )
    })

    return next.handle(localStorage.getItem("token") !== null ? authRequest : request);
  }
}
