import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from '../../environments/environment';

@Injectable({
	providedIn: 'root'
})

export class BarrioService {
	private alive: boolean = true;
	
	constructor(private http: HttpClient) {
	}

	findAllBarrios(){
		return this.http.post(environment.apiUrl + "Barrio/findAll/", {});
	}

	insertBarrio(barrio){
		return this.http.post(environment.apiUrl + "Barrio/", barrio);
	}

	updateBarrio(barrio){
		return this.http.post(environment.apiUrl + "Barrio/updateBarrio", barrio);
	}

	deleteBarrio(barrio){
		return this.http.post(environment.apiUrl + "Barrio/deleteBarrio", barrio);
	}

	uploadBarrio(file){
		return this.http.put(environment.apiUrl + "Barrio/uploadBarrio", file);
	}
}