import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Http } from '@angular/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { SecurityGuard } from './guard/security.guard';
import { NoSecurityGuard } from './guard/noSecurity.guard';
import { DomSeguroPipe } from './pipes/domseguro.pipe';

import { AngularFontAwesomeModule } from 'angular-font-awesome';

import {NgbPaginationModule, NgbPaginationConfig} from '@ng-bootstrap/ng-bootstrap';

//services
import {
    ApiInterceptor,
    InstitucionesService,
    BarrioService,
    OcupacionService,
    UsuarioService,

} from './services';

//component
import {
    InstitucionesComponent,
    BarrioComponent,
    OcupacionComponent,
    MenuComponent,
    LoginComponent,
    EgresadosComponent
} from "./components";

@NgModule({
  declarations: [
    AppComponent,
    InstitucionesComponent,
    BarrioComponent,
    OcupacionComponent,
    MenuComponent,
    LoginComponent,
    EgresadosComponent,
    //pipes
    DomSeguroPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    NgbPaginationModule
  ],
  providers: [
      NgbPaginationConfig,
      InstitucionesService,
      BarrioService,
      OcupacionService,
      UsuarioService,
      SecurityGuard,
      NoSecurityGuard,
      Http,
      { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
