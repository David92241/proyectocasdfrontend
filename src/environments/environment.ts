let production = false;

function getAPI(isProduccion) {
    if (isProduccion) {
        return 'https://api.novaip.us/novaip-common-ws/api/';
    }
    return "http://localhost:8080/ProyectoCasdBackEnd-1.0-SNAPSHOT/rest/";
}

export const environment = {
    production: production,
    apiUrl: getAPI(production)
};
